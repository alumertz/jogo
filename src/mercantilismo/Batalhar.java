package mercantilismo;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Classe responsável pela lógica da batalha entre os exércitos.
 * @author Ana e Mateus
 */
public class Batalhar {
    /**
     * ArrayList usado para armazenar os dois exércitos dpeois de criados.
     */
    private static ArrayList<Exercito> vetExercito = new ArrayList<>();
    private static int  qCav, qArq, qCur, qMer,nivel;
    
    /**
     * Método que recebe o nível da batalha escolhido pelo usuário.
     * @param niv int - Nivel
     */
    public static void setNivel(int niv){
        nivel = niv;        
    }
    /**
     * Método que limpa o vetor de exércitos.
     */
    public static void limparBatalha(){
        vetExercito = new ArrayList<>();
    }
    /**
     * Método que recebe a quantidade de cada unidade do exército montado pelo usuário.
     * @param qCavaleiro int - Quantidade de cavaleiros.
     * @param qArqueira int - Quantidade de arqueiras.
     * @param qCurandeiro int - Quantidade de curandeiros.
     * @param qMercenario int - Quantidade de mercenarios.
     */
    public static void setDados(int qCavaleiro, int qArqueira, int qCurandeiro, int qMercenario){
        qCav = qCavaleiro;
        qArq = qArqueira;
        qCur = qCurandeiro;
        qMer = qMercenario;
    }
    private static int random(int min, int max){
        int range = (max - min) +1;
        return (int)(Math.random() *range)+min;
    }
    /**
     * Método que sorteia o exército PC de acordo com o nível escolhido pelo usuário.
     */
    private static void sorteiaExercito(){
        double porCemMin =0, porCemMax=0;
        switch (nivel){
            case 0: 
                porCemMin = 0.2;
                porCemMax = 0.4;
            break;
            case 1:
                porCemMin = 0.3;  
                porCemMax = 0.5;
            break;
            case 2:
                porCemMin = 0.4;  
                porCemMax = 0.6;
            break;
            case 3:
                porCemMin = 0.6;  
                porCemMax = 0.8;
            break;
        }
        
        qCav = random ((qCav+(int)(porCemMin*qCav)),(qCav+(int)(porCemMax*qCav)));
        qArq = random ((qArq+(int)(porCemMin*qArq)),(qArq+(int)(porCemMax*qArq)));
        qCur = random ((qCur+(int)(porCemMin*qCur)),(qCur+(int)(porCemMax*qCur)));
        qMer = random ((qMer+(int)(porCemMin*qMer)),(qMer+(int)(porCemMax*qMer)));
    }

    /**
     * Método que cria os exércitos que irão batalhar, adicionando as unidades em um arraylist.
     * @return  ArrayList&lt;Exercito&gt; - array com os exércitos
     */
    public static ArrayList<Exercito> criarExercitos(){
        vetExercito.add(new Exercito());
        vetExercito.add(new Exercito());
        
        for (int exercito=0; exercito<2;exercito++){
            if (exercito==1){
                sorteiaExercito();
            }
            for (int x =0;x<qCav;x++){
                vetExercito.get(exercito).adicionar(new Cavaleiro());
            }
            for (int x =0;x<qArq;x++){
                vetExercito.get(exercito).adicionar(new Arqueira());            
            }
            for (int x =0;x<qCur;x++){
                vetExercito.get(exercito).adicionar(new Curandeiro());            
            }
            for (int x =0;x<qMer;x++){
                vetExercito.get(exercito).adicionar(new Mercenario());            
            }            
            Collections.shuffle (vetExercito.get(exercito).getVetPersonagem());
        }
        for (int x=0;x<vetExercito.get(0).getVetSize();x++){
            vetExercito.get(0).getVetPersonagem().get(x).adicionarVida();
        }
        return vetExercito;
    }
    
    /////////////Batalha/////////////

    /**
     * Método que verifica e retorna o vencedor da batalha.
     * @return boolean - Se usuário venceu retona true, se perdeu retorna false.
     */        
    public static boolean getVencedor(){
        
        ArrayList <Exercito> exercitos = lutar (vetExercito);
        int mortos1 = exercitos.get(0).getContMortos();
        int mortos2 = exercitos.get(1).getContMortos();
        System.out.println("mortos 1 "+ mortos1);
        System.out.println("mortos 2 "+ mortos2);
        if (mortos1< mortos2){
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Método responsável por remover os mortos.
     * @return ArrayList<Exercito> - array com os exercitos alterados
     */
    private static ArrayList<Exercito> enterrar(ArrayList<Exercito> exercito){
        for (int x=0; x<2;x++){
            for (int y = exercito.get(x).getVetSize()-1; y>=0;y--){
                if (exercito.get(x).getVetPersonagem().get(y).isDead()){
                    exercito.get(x).getVetPersonagem().remove(y);
                }
            }
        }
        return exercito;
    }
    /**
     * Método responsável pela batalha entre os dois exércitos.
     * @param exercito - array com os exercitos
     * @return ArrayList<Exercito> - array com os exercitos
     */
    private static ArrayList<Exercito> lutar(ArrayList<Exercito> exercito){
        ArrayList<Exercito> especial;
        int mQuant = Math.min(exercito.get(0).getVetSize(), exercito.get(1).getVetSize());
        do{
            for (int x=mQuant-1;x>=0;x--){
                Personagem sobrevivente;
                int danoUsuario = exercito.get(0).getPersonagem(x).getDano();
                int danoPC = exercito.get(1).getPersonagem(x).getDano();
                exercito.get(0).getPersonagem(x).removerVida(danoPC);
                exercito.get(1).getPersonagem(x).removerVida(danoUsuario);
                for (int y=0; y<2; y++){
                    if (exercito.get(y).getPersonagem(x).isDead()){
                        exercito.get(y).addContMortos();
                        int z;
                        if (y==0){z=1;}
                        else{z=0;}
                        sobrevivente = exercito.get(z).getPersonagem(x);
                        especial = sobrevivente.especial(exercito.get(z), exercito.get(y));
                        if (y==0){
                            exercito.add(0, especial.get(1));
                            exercito.add(1, especial.get(0));
                        }
                        else{
                            exercito.add(0, especial.get(0));
                            exercito.add(1, especial.get(1));
                        }
                    } 
                }                
            }
            exercito = enterrar(exercito);
            mQuant = Math.min(exercito.get(0).getVetSize(), exercito.get(1).getVetSize());
        }while (mQuant!=0);
        return exercito;
    }
}
