package mercantilismo;

import java.util.ArrayList;

/**
 * Classe que armazena os componentes Personagem de um determinado exército em um Arraylist. 
 * Guarda o custo do exército.
 * @author Ana e Mateus
 */
public class Exercito {
    private ArrayList<Personagem> vetPersonagem;
    private int contMortos=0;

    /**
     * Construtor do exército, inicializa o vetor de personagens.
     */
    public Exercito() {
        vetPersonagem = new ArrayList<>();
    }
    
    /**
     * Método que adiciona o componente Personagem de um exército no Arraylist.
     * @param p Personagem - personagem do exército
     */
    public void adicionar (Personagem p){
        vetPersonagem.add(p);
    }

    /**
     * Método que retorna o vetor de personagens.
     * @return ArrayList&lt;Personagem&gt; - vetor de personagens
     */
    public ArrayList<Personagem> getVetPersonagem() {
        return vetPersonagem;
    }

    /**
     * Método recebe um vetor de personagens e copia para o vetor de personagens próprio.
     * @param vetPersonagem ArrayList&lt;Personagem&gt; - vetor de personagens
     */
    public void setVetPersonagem(ArrayList<Personagem> vetPersonagem) {
        this.vetPersonagem = vetPersonagem;
    }
    
    /**
     * Método retorna o tamanho do vetor de personagens.
     * @return int - tamanho vetor de personagens.
     */
    public int getVetSize() {
        return this.vetPersonagem.size();
    }

    /**
     * Método que retorna um personagem do exércto.
     * @param p int - número do personagem no array
     * @return Personagem - personagem
     */
    public Personagem getPersonagem (int p){
        return this.vetPersonagem.get(p);
    }
    /**
     * Retorna quantidade de personagens mortos do exército.
     * @return int - quantidade de mortos
     */
    public int getContMortos() {
        return contMortos;
    }
    /**
     * Adiciona um morto na contagem de mortos desse exército.
     */
    public void addContMortos() {
        this.contMortos ++;
    }

    
    
}
