package mercantilismo;

import java.util.ArrayList;

/**
 * Classe que representa a unidade mercenario no jogo.
 * @author Mateus
 */
public class Mercenario extends Personagem{
    private static int pontosVidaCustomizaveis=0, pontosDanoCustomizaveis=0;

    /**
     * Construtor que define a quantidade de vida, dano e custo padrão para o construtor super.
     * Passa uma string com o nome da unidade Mercenario.
     */
    public Mercenario (){
        super (140,50,10,"Mercenario");
    }

    /**
     * Método responsavel pelo golpe especial da unidade mercenario.
     * O Exercito perdedor perde 5 pontos de dano de cada unidade.
     * @param vencedor Exercito - Exercito que teve a unidade vencedora.
     * @param perdedor Exercito - Exercito que teve a unidade perdedora.
     * @return ArrayList&lt;Exercito&gt; - Retorna um array com os exercitos alterados.
     */
    @Override
    public ArrayList<Exercito> especial(Exercito vencedor, Exercito perdedor){
        for (int x=0; x<perdedor.getVetPersonagem().size(); x++){
            perdedor.getVetPersonagem().get(x).setDano(perdedor.getVetPersonagem().get(x).getDano()- 5);
        }
        ArrayList<Exercito> resultado = new ArrayList<>();
        resultado.add(vencedor);
        resultado.add(perdedor);
        return resultado;
    }
    
    /**
     * Método que soma a vida base com os pontos de vida customizáveis definidos pelo usuário.
     */
    @Override
    public void adicionarVida(){
        setVida(this.pontosVidaCustomizaveis+this.getVida());
    }
    
    /**
     * Método que retorna os pontos de vida da Mercenario customizáveis definidos pelo usuário.
     * @return int - pontos de vida definidos para a Mercenario
     */
    @Override
    public int getPontosVidaCustomizaveis() {
        return pontosVidaCustomizaveis;
    }
    
    /**
     * Método que modifica os pontos de vida customizáveis definidos pelo usuário.
     * @param pontosVidaCustomizaveis int - pontos de vida definidos pelo usuário
     */
    @Override
    public void setPontosVidaCustomizaveis(int pontosVidaCustomizaveis) {
        Mercenario.pontosVidaCustomizaveis = pontosVidaCustomizaveis;
    }
    
    /**
     * Método que retorna os pontos de dano customizáveis definidos pelo usuário.
     * @return int - pontos de dano definidos para a Mercenario
     */
    @Override
    public int getPontosDanoCustomizaveis() {
        return pontosDanoCustomizaveis;
    }

    /**
     * Método que modifica os pontos de dano customizáveis definidos pelo usuário.
     * @param pontosDanoCustomizaveis int - pontos de dano da Mercenario
     */
    @Override
    public void setPontosDanoCustomizaveis(int pontosDanoCustomizaveis) {
        Mercenario.pontosDanoCustomizaveis = pontosDanoCustomizaveis;
    }
    

}
