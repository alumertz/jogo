
package mercantilismo;

import java.util.ArrayList;

/**
 * Classe que representa a unidade curandeiro no jogo
 * @author Ana e Mateus
 */
public class Curandeiro extends Personagem{
    private static int pontosVidaCustomizaveis=0, pontosDanoCustomizaveis=0;

    /**
     * Construtor que passa a quantidade de vida, dano e custo padrão para o construtor super.
     * Passa uma string com o nome da unidade Curandeiro.
     */
    public Curandeiro (){
        super (230,40,13, "Curandeiro");
    }

    /**
     * Método responsavel pelo golpe especial da unidade curandeiro
     * O Exercito vencedor ganha 10 pontos de vida para cada unidade.
     * @param vencedor Exercito - Exercito vencedor
     * @param perdedor Exercito - Exercito perdedor
     * @return ArrayList&lt;Exercito&gt; - Retorna um array com os exercitos alterados.
     */
    @Override
    public ArrayList<Exercito> especial(Exercito vencedor, Exercito perdedor){
        for (int x=0; x<vencedor.getVetPersonagem().size(); x++){
            vencedor.getVetPersonagem().get(x).setVida(vencedor.getVetPersonagem().get(x).getVida()+ 10);
        }
        ArrayList<Exercito> resultado = new ArrayList<>();
        resultado.add(vencedor);
        resultado.add(perdedor);
        return resultado;
    }

    /**
     * Método que soma a vida base com os pontos de vida customizáveis definidos pelo usuário.
     */
    @Override
    public void adicionarVida(){
        setVida(this.pontosVidaCustomizaveis+this.getVida());
    }

    /**
     * Método que retorna os pontos de vida do Curandeiro definidos pelo usuário.
     * @return int - Retorna os pontos de vida customizáveis definidos para o Curandeiro.
     */
    @Override
    public int getPontosVidaCustomizaveis() {
        return pontosVidaCustomizaveis;
    }

    /**
     * método que modifica os pontos de vida do Curandeiro.
     * @param pontosVidaCustomizaveis int - Pontos de vida customizáveis definidos pelo usuário.
     */
    @Override
    public void setPontosVidaCustomizaveis(int pontosVidaCustomizaveis) {
        Curandeiro.pontosVidaCustomizaveis = pontosVidaCustomizaveis;
    }

    /**
     * Método que retorna os pontos de dano definidos pelo usuário.
     * @return int - Retorna os pontos de dano customizáveis definidos para o Curandeiro.
     */
    @Override
    public int getPontosDanoCustomizaveis() {
        return pontosDanoCustomizaveis;
    }

    /**
     * Método que modifica os pontos de dano definidos pelo usuário.
     * @param pontosDanoCustomizaveis int - Pontos de dano customizáveis do Curandeiro.
     */
    @Override
    public void setPontosDanoCustomizaveis(int pontosDanoCustomizaveis) {
        Curandeiro.pontosDanoCustomizaveis = pontosDanoCustomizaveis;
    }

    
}
