package mercantilismo;

import imagens.LoaderImagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Classe que controla a tela de informações das unidades
 *
 * @author Ana e Mateus
 */
public class StatusPersonagensController implements Initializable {
    @FXML
    private ImageView imgPersonagem;
    
    @FXML
    private Label dano;
    
    @FXML
    private Label vida;
    
    @FXML
    private Label especial;
    
    Personagem pers;

    /**
     * Método que coloca na tela de status as informações e a imagem da unidade passada como parâmetro.
     * @param nomePersonagem String - nome da classe do personagem
     */
    public void setPersonagens(String nomePersonagem){
        switch (nomePersonagem){
            case "cavaleiro":
                pers = new Cavaleiro();
                Image image = new Image(LoaderImagem.load("fundoCav.png"));
                imgPersonagem.setImage(image);
                especial.setText("Adiciona 5 de dano para o \nexército aliado ao matar um inimigo");
                break;
            case "arqueira":
                pers = new Arqueira();
                Image image2 = new Image(LoaderImagem.load("fundoArq.png"));
                imgPersonagem.setImage(image2);
                especial.setText("Retira 5 de vida do \nexército rival ao matar um inimigo");
                break;
            case "curandeiro":
                pers = new Curandeiro();
                Image image3 = new Image(LoaderImagem.load("fundoCuran.png"));
                imgPersonagem.setImage(image3);
                especial.setText("Cura 10 de vida do \nexército aliado ao matar um inimigo");
                break;
            case "mercenario":
                pers = new Mercenario();
                Image image4 = new Image(LoaderImagem.load("fundoMerc.png"));
                imgPersonagem.setImage(image4);
                especial.setText("Retira 5 de dano do \nexército rival ao matar um inimigo");
                break;
        }
        vida.setText(Integer.toString(pers.getVida()));
        dano.setText(Integer.toString(pers.getDano()));
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    /**
     * Método chama o troca a tela para o menu.
     */
    @FXML
    private void voltar(){
        Mercantilismo.trocaTela("Menu.fxml");
        
    }
}
