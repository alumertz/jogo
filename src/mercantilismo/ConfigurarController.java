package mercantilismo;

import imagens.LoaderImagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Classe que controla a configuração das unidades.
 * @author Ana e Mateus
 */
public class ConfigurarController implements Initializable {
    @FXML
    private ImageView imgPersonagem;
    
    @FXML
    private Label vida,dano,pontos, aviso;
    
    @FXML
    private TextField setVida,setDano;
    
    Personagem pers;

    /**
     * Método que coloca na tela de configuração as informações e a imagem da unidade passada como parâmetro.
     * @param nomePersonagem String - classe do personagem
     */
    public void setPersonagens(String nomePersonagem){
        switch (nomePersonagem){
            case "cavaleiro":
                pers = new Cavaleiro();
                Image image = new Image(LoaderImagem.load("fundoCav.png"));
                imgPersonagem.setImage(image);
                break;
            case "arqueira":
                pers = new Arqueira();
                Image image2 = new Image(LoaderImagem.load("fundoArq.png"));
                imgPersonagem.setImage(image2);
                break;
            case "curandeiro":
                pers = new Curandeiro();
                Image image3 = new Image(LoaderImagem.load("fundoCuran.png"));
                imgPersonagem.setImage(image3);
                break;
            case "mercenario":
                pers = new Mercenario();
                Image image4 = new Image(LoaderImagem.load("fundoMerc.png"));
                imgPersonagem.setImage(image4);
                break;
        }
        vida.setText(Integer.toString(pers.getVida()+ pers.getPontosVidaCustomizaveis()));
        dano.setText(Integer.toString(pers.getDano()+ pers.getPontosDanoCustomizaveis()));
        pontos.setText(Integer.toString(pers.getPontos() - (pers.getPontosDanoCustomizaveis()+pers.getPontosVidaCustomizaveis())));
    }
    /**
     * Método responsável pela verificação dos pontos que podem ser adicionados a unidade, fazendo o teste para que o usuário não tire a vida e dano padrão da unidade ou que adicione mais pontos do que pode.
     * @param select String - selecionar: vida ou dano
     * @param opcao String - opção: remover ou adicionar
     * @param quant int - quantidade de vida ou dano escolhida pelo usuário
     * @return boolean - foi possível adicionar/reitrar os pontos
     */
    private boolean verificarPontos(String select, String opcao, int quant){
        boolean retorna=false;
        if (opcao =="remover"){
            if(select=="vida"){
                if (pers.getPontosVidaCustomizaveis()>=quant){
                    retorna = true;
                } 
            }
            else if (select=="dano"){
                if (pers.getPontosDanoCustomizaveis()>=quant){
                    retorna = true;
                } 
            }
            else{aviso.setText("Você não pode retirar valores base!");}
        }
        else if (opcao=="adicionar"){
            if(pers.getPontos()-(pers.getPontosVidaCustomizaveis()+ pers.getPontosDanoCustomizaveis())>=quant){
                retorna = true;
            }
            else if (pers.getPontos()-(pers.getPontosVidaCustomizaveis()+ pers.getPontosDanoCustomizaveis())>=quant){
                retorna = true;
            }
        }
        if (retorna == true){
            aviso.setText("Operação feita com sucesso!");
        }
        else{
            aviso.setText("A operação não pode ser concluida, verique os valores inseridos.");
        }
        return retorna;
    }
    @FXML
    /**
     * Método que adiciona a vida para a unidade de acordo com os pontos digitados pelo usuário.
     */
    private void adicionarVida(){
        if (verificarPontos("vida","adicionar",Integer.parseInt(setVida.getText()))){
            pers.setPontosVidaCustomizaveis(pers.getPontosVidaCustomizaveis() + Integer.parseInt(setVida.getText()));
            vida.setText(Integer.toString(pers.getPontosVidaCustomizaveis()+ pers.getVida()));
            pontos.setText(Integer.toString(pers.getPontos()-(pers.getPontosVidaCustomizaveis()+ pers.getPontosDanoCustomizaveis())));
        }
    }
    @FXML 
    /**
     * Método que adiciona o dano para a unidade de acordo com os pontos digitados pelo usuário.
     */
    private void adicionarDano(){
        if (verificarPontos("dano","adicionar",Integer.parseInt(setDano.getText()))){
            pers.setPontosDanoCustomizaveis(pers.getPontosDanoCustomizaveis() + Integer.parseInt(setDano.getText()));
            dano.setText(Integer.toString(pers.getPontosDanoCustomizaveis()+ pers.getDano()));
            pontos.setText(Integer.toString(pers.getPontos()- (pers.getPontosDanoCustomizaveis()+pers.getPontosVidaCustomizaveis())));
        }
    }
    @FXML
    /**
     * Método que remove a vida da unidade de acordo com os pontos digitados pelo usuário. 
     */
    private void removerVida(){
        if (verificarPontos("vida","remover",Integer.parseInt(setVida.getText()))){
            pers.setPontosVidaCustomizaveis(pers.getPontosVidaCustomizaveis() - Integer.parseInt(setVida.getText()));
            vida.setText(Integer.toString(pers.getPontosVidaCustomizaveis()+ pers.getVida()));
            pontos.setText(Integer.toString(pers.getPontos()-(pers.getPontosVidaCustomizaveis()+ pers.getPontosDanoCustomizaveis())));
        }
    }
    /**
     * Método que remove o dano da unidade de acordo com os pontos digitados pelo usuário. 
     */
    @FXML 
    private void removerDano(){
         
        if (verificarPontos("dano","remover", Integer.parseInt(setDano.getText()))){
            pers.setPontosDanoCustomizaveis(pers.getPontosDanoCustomizaveis() - Integer.parseInt(setDano.getText()));
            dano.setText(Integer.toString(pers.getPontosDanoCustomizaveis()+ pers.getDano()));
            pontos.setText(Integer.toString(pers.getPontos()-(pers.getPontosVidaCustomizaveis()+ pers.getPontosDanoCustomizaveis())));
        }
    }
    /**
     * Método que troca para a tela do menu principal do jogo.
     */
    @FXML
    private void configurado(){
        Mercantilismo.trocaTela("Menu.fxml");
        
    }
     @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
