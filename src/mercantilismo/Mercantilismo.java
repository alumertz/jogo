package mercantilismo;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Ana e Mateus
 */
public class Mercantilismo extends Application {

    /**
     * Controller da tela de configuração
     */
    public static ConfigurarController controllerConfigurar;

    /**
     * Controller da tela de resultados da batalha
     */
    public static BatalhaController controllerBatalha;

    /**
     * Controller da tela de status dos persoangens
     */
    public static StatusPersonagensController controllerStatus;
    private static Stage stage;

    /**
     * Método que retorna o stage
     * @return Stage
     */
    public static Stage getStage() {
        return stage;
    }
    
    /**
     * Método troca tela simples.
     * @param tela String - próxima tela
     */
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(Mercantilismo.class.getResource(tela));
            Scene scene = new Scene(root);
        
            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
            e.printStackTrace();
        }
        
    }
    
    /**
     * Método troca tela com vetor dos exércitos.
     * @param tela String - próxima tela
     * @param vetExercito ArrayList&lt;Exercito&gt; - lista com os exércitos
     */
    public static void trocaTelaArray(String tela, ArrayList<Exercito> vetExercito){
        Parent root = null;
        try{
            root = FXMLLoader.load(Mercantilismo.class.getResource(tela));
            Scene scene = new Scene(root);
            
        
            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
            e.printStackTrace();
        }
    }

    /**
     * Método troca tela com o parâmetro nível.
     * @param tela String - próxima tela
     * @param nivel int - nível do jogo
     */
    public static void trocaTelaNivel(String tela, int nivel){
        Parent root = null;
        try{
            FXMLLoader loader = new FXMLLoader(Mercantilismo.class.getResource(tela));
            root = loader.load();
            Scene scene = new Scene(root);
            Batalhar.setNivel(nivel);
            ArrayList<Exercito> vetExercito = Batalhar.criarExercitos();
            boolean vencedor = Batalhar.getVencedor();
            controllerBatalha = (BatalhaController)loader.getController();
            controllerBatalha.setVencedor(vencedor, vetExercito);
            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
            e.printStackTrace();
        }
        
    }
    
    /**
     * Método troca tela com parâmetro da classe do personagem.
     * @param tela String - próxima tela
     * @param personagem String - classe do personagem
     */
    public static void trocaTelaPersonagens(String tela, String personagem){
        Parent root = null;
        try{
            FXMLLoader loader = new FXMLLoader(Mercantilismo.class.getResource(tela));
            root = loader.load();            
            Scene scene = new Scene(root);
            switch (tela){
                case "Configurar.fxml": 
                    controllerConfigurar = (ConfigurarController)loader.getController();
                    controllerConfigurar.setPersonagens(personagem);
                break;
                case "StatusPersonagens.fxml":
                    controllerStatus = (StatusPersonagensController)loader.getController();
                    controllerStatus.setPersonagens(personagem);
                break;
            }                       
            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML "+ e);
            e.printStackTrace();
        }
    }

    /**
     * Método troca tela com variáveis de quantidade e custo.
     * @param tela String - próxima tela
     * @param qCav int - quantidade de cavaleiros
     * @param qArq int - quantidade de arqueiras
     * @param qCur int - quantidade de curandeiros
     * @param qMer int - quantidade de mercenarios
     */
    public static void trocaTelaQuantidade(String tela, int qCav, int qArq, int qCur, int qMer){
        Parent root = null;
        try{
            FXMLLoader loader = new FXMLLoader(Mercantilismo.class.getResource(tela));
            root = loader.load();
            
            Scene scene = new Scene(root);
                        
            Batalhar.setDados(qCav, qArq,qCur, qMer);
                        
            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML "+ e);
            e.printStackTrace();

        }
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
        
        Scene scene = new Scene(root);
        
        Mercantilismo.stage = stage;
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Método main
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
