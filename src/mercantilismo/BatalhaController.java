package mercantilismo;

import imagens.LoaderImagem;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
 
/**
 * Classe que mostra os dados da batalha na tela.
 * @author Ana e Mateus
 */
public class BatalhaController implements Initializable {
    
    ArrayList<Exercito> vetExercito;
    @FXML
    ImageView resultado;
    @FXML
    Label matou,morreu;
        
    /**
     * Método mostra na tela se você ganhou ou perdeu a batalha.
     * @param ven int - Variável com resultado 
     * @param vet ArrayList&lt;Exercito&gt; - array com os exercitos
     */
    public void setVencedor (boolean ven, ArrayList<Exercito> vet){
        vetExercito = vet;
        if(ven){
            Image imageVit = new Image(LoaderImagem.load("fundoVitoria.png"));
            resultado.setImage(imageVit);
        }
        else{
            Image imageDerr = new Image(LoaderImagem.load("fundoDerrota.png"));
            resultado.setImage(imageDerr);
        }
        matou.setText("Você matou "+ vetExercito.get(1).getContMortos()+ " unidades");
        morreu.setText("Você teve "+ vetExercito.get(0).getContMortos()+ " unidades mortas");
        
    }/**
     * Método limpa a batalha e troca de tela.
     */
    @FXML
    private void menu(){
        Batalhar.limparBatalha();
        Mercantilismo.trocaTela("Menu.fxml");
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}
