package mercantilismo;

import java.util.ArrayList;

/**
 * Classe que representa a unidade cavaleiro no jogo.
 * @author Ana e Mateus
 */
public class Cavaleiro extends Personagem{
    private static int pontosVidaCustomizaveis=0, pontosDanoCustomizaveis=0;

    /**
     * Construtor que define a quantidade de vida, dano e custo padrão para o construtor super.
     * Passa uma string com o nome da unidade Cavaleiro.
     */
    public Cavaleiro (){
        super (150,55,15,"Cavaleiro");
    }

    /**
     * Método responsavel pelo golpe especial da unidade cavaleiro.
     * O Exercito vencedor ganha 5 pontos de dano para cada unidade.
     * @param vencedor Exercito - Exercito que teve a unidade vencedora.
     * @param perdedor Exercito - Exercito que teve a unidade perdedora.
     * @return ArrayList&lt;Exercito&gt; - Retorna um vetor com os exercitos alterados.
     */
    @Override
    public ArrayList<Exercito> especial(Exercito vencedor, Exercito perdedor){
        for (int x=0; x<vencedor.getVetPersonagem().size(); x++){
            vencedor.getVetPersonagem().get(x).setDano(vencedor.getVetPersonagem().get(x).getDano()+ 5);
        }
        ArrayList<Exercito> resultado = new ArrayList<>();
        resultado.add(vencedor);
        resultado.add(perdedor);
        return resultado;
    } 

    /**
     * Método que soma a vida base com os pontos de vida customizáveis definidos pelo usuário
     */
    @Override
    public void adicionarVida(){
        setVida(this.pontosVidaCustomizaveis+this.getVida());
    }
/**
     * Método que retorna os pontos de vida da Cavaleiro customizáveis definidos pelo usuário.
     * @return int - pontos de vida definidos para a Cavaleiro
     */
    @Override
    public int getPontosVidaCustomizaveis() {
        return pontosVidaCustomizaveis;
    }
    
    /**
     * Método que modifica os pontos de vida customizáveis definidos pelo usuário.
     * @param pontosVidaCustomizaveis int - pontos de vida definidos pelo usuário
     */
    @Override
    public void setPontosVidaCustomizaveis(int pontosVidaCustomizaveis) {
        Cavaleiro.pontosVidaCustomizaveis = pontosVidaCustomizaveis;
    }
    
    /**
     * Método que retorna os pontos de dano customizáveis definidos pelo usuário.
     * @return int - pontos de dano definidos para a Cavaleiro
     */
    @Override
    public int getPontosDanoCustomizaveis() {
        return pontosDanoCustomizaveis;
    }

    /**
     * Método que modifica os pontos de dano customizáveis definidos pelo usuário.
     * @param pontosDanoCustomizaveis int - pontos de dano da Cavaleiro
     */
    @Override
    public void setPontosDanoCustomizaveis(int pontosDanoCustomizaveis) {
        Cavaleiro.pontosDanoCustomizaveis = pontosDanoCustomizaveis;
    }
}
