package mercantilismo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * Classe que controla a seleção da dificuldade da batalha.
 * @author Ana e Mateus
 */
public class SelecionarController implements Initializable {
    int nivel;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    
    /**
     * Método que define a batalha na dificuldade fácil.
     */
    @FXML
    public void inimigoFacil() {
        nivel =0;
        Mercantilismo.trocaTelaNivel("Batalha.fxml",nivel);
    }

    /**
     * Método que define a batalha na dificuldade média.
     */
    @FXML
    public void inimigoMedio() {
        nivel =1;
        Mercantilismo.trocaTelaNivel("Batalha.fxml",nivel);
    }

    /**
     * Método que define a batalha na dificuldade difícil.
     */
    @FXML
    public void inimigoDificil() {
        nivel =2;
        Mercantilismo.trocaTelaNivel("Batalha.fxml",nivel);
    }

    /**
     * Método que define a batalha na dificuldade expert.
     */
    @FXML
    public void inimigoExpert() {
        nivel =3;
        Mercantilismo.trocaTelaNivel("Batalha.fxml",nivel);
    }

    
}
