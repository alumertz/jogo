package mercantilismo;

import java.util.ArrayList;

/**
 * Classe que define atributos e métodos gerais dos personagens definidos nas classes Cavaleiro, Arqueira, Curandeiro e Mercenario.
 * @author Ana e Mateus
 */
public abstract class Personagem {
    private static int pontos;
    private int vida,dano, custo;
    private String classe;
            
    /** 
     * Construtor que define a quantidade de vida, dano, custo e pontos padrão para os personagens.
     * @param vida int - Vida do personagem.
     * @param dano int - Dano do personagem.
     * @param custo int - Custo do personagem.
     * @param classe String - Classe do personagem.
     */
    public Personagem(int vida, int dano, int custo, String classe){
        setVida (vida);
        setDano (dano);
        setCusto(custo);
        setPontos(10);
        setClasse(classe);
    }

    /**
     * Método que retorna a classe.
     * @return String
     */
    public String getClasse() {
        return classe;
    }

    /**
     * Método que modifica a classe.
     * @param classe String
     */
    public void setClasse(String classe) {
        this.classe = classe;
    }
    
    /**
     * Método que retorna o custo dos personagens.
     * @return int
     */
    public int getCusto() {
        return custo;
    }

    /**
     * Método que modifica o custo dos personagens.
     * @param custo int
     */
    public void setCusto(int custo) {
        this.custo = custo;
    }

    /** 
     * método que retorna os pontos dos personagens.
     * @return int
     */
    public static int getPontos() {
        return pontos;
    }

    /**
     * Método que modifica o custo dos personagens.
     * @param pontos - int
     */
    public static void setPontos(int pontos) {
        Personagem.pontos = pontos;
    }

    /**
     * Método que retorna a vida dos personagens.
     * @return int
     */
    public int getVida() {
        return vida;
    }

    /**
     * Método que modifica a vida dos personagens.
     * @param vida - int
     */
    public void setVida(int vida) {
        this.vida = vida;
    }

    /**
     * Método que retorna o dano dos personagens.
     * @return int
     */
    public int getDano() {
        return dano;
    }

    /**
     * Método que modifica o dano dos personagens.
     * @param dano - int
     */
    public void setDano(int dano) {
        this.dano = dano;
    }
        
    /**
     * Método que recebe o dano a a ser removido da vida dos personagens.
     * @param dano - int
     */
    public void removerVida (int dano){
        this.vida -=dano;
    }
    
    /**
     * Método que verifica se o personagem está morto.
     * @return boolean - Retorna true para morte e false para vivo.
     */
    public boolean isDead(){
        return vida<0;
    }
    
    /**
     * Método que soma vida e pontos de vida.
     */
    public abstract void adicionarVida();
    
    /**
     * Método que retorna pontos de vida.
     * @return int - pontos de vidacustomizáveis
     */
    public abstract int getPontosVidaCustomizaveis();

    /**
     * Método que modifica os pontos de vida.
     * @param pontosVidaCustomizaveis int - pontos de vida customizáveis
     */
    public abstract void setPontosVidaCustomizaveis(int pontosVidaCustomizaveis);

    /**
     * Método que retorna pontos de dano.
     * @return int - pontos de dano customizáveis
     */
    public abstract int getPontosDanoCustomizaveis();

    /**
     * Método que modifica os pontos de dano.
     * @param pontosDanoCustomizaveis int - pontos de dano customizáveis
     */
    public abstract void setPontosDanoCustomizaveis(int pontosDanoCustomizaveis);
    
    /**
     * Método que executa o atque especial.
     * @param vencedor Exercito
     * @param perdedor Exercito
     * @return ArrayList&lt;Exercito&gt;
     */
    public abstract ArrayList<Exercito> especial(Exercito vencedor, Exercito perdedor);
    
}
