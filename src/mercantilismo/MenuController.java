
package mercantilismo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Classe que controla o menu principal do jogo.
 * @author Ana e Mateus
 */
public class MenuController implements Initializable {
    
    @FXML
    private TextField quantCavaleiro,quantArqueira,quantCurandeiro,quantMercenario;
    @FXML
    private Label dinheiroTela,custoCavaleiro,custoArqueira,custoCurandeiro,custoMercenario,aviso;
    int dinheiro = 5000, cCav = 15, cArq =12, cCur = 13, cMer=10;
    /**
     * Inicializa a tela, utiliza um listener para a cada mudança recalcular o custo do exército.
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        dinheiroTela.setText(Integer.toString(dinheiro));
        custoCavaleiro.setText(Integer.toString(cCav));
        custoArqueira.setText(Integer.toString(cArq));
        custoCurandeiro.setText(Integer.toString(cCur));
        custoMercenario.setText(Integer.toString(cMer));
        quantCavaleiro.textProperty().addListener((final ObservableValue<? extends String> observable, final String oldValue, final String newValue) -> {
           calcularCusto(); 
        });
        quantArqueira.textProperty().addListener((final ObservableValue<? extends String> observable, final String oldValue, final String newValue) -> {
            calcularCusto(); 
        });
        quantCurandeiro.textProperty().addListener((final ObservableValue<? extends String> observable, final String oldValue, final String newValue) -> {
            calcularCusto(); 
        });
        quantMercenario.textProperty().addListener((final ObservableValue<? extends String> observable, final String oldValue, final String newValue) -> {
            calcularCusto(); 
        });        
    }    
        
    /**
     * Método que lê a quantidade de cada unidade e avança para a tela de seleção de dificuldade.
     */
    @FXML
    public void batalhar(){
        int soma, custo;
        int qCavaleiro = Integer.parseInt(quantCavaleiro.getText());
        int qArqueira = Integer.parseInt(quantArqueira.getText());
        int qCurandeiro = Integer.parseInt(quantCurandeiro.getText());
        int qMercenario = Integer.parseInt(quantMercenario.getText());
        soma = qCavaleiro + qArqueira + qCurandeiro + qMercenario;
        custo = calcularCusto();
        if (custo<=dinheiro && soma>0){
            Mercantilismo.trocaTelaQuantidade("SelecionarInimigo.fxml",qCavaleiro, qArqueira,qCurandeiro,qMercenario);
        }
        else if (soma<=0){
            aviso.setText("Número de personagens inválido!");
        }
        else{
            aviso.setText("Você não tem dinheiro suficiente!");
        }
    }

    /**
     * Método que calcula o custo total de cada unidade e modifica o dinheiro na tela.
     * @return int - custo do exército com determinada quantidade de personagens
     */
    public int calcularCusto(){
        int custo = 0;
        String textCav, textArq,textCur,textMer;
        textCav = quantCavaleiro.getText();
        textArq = quantArqueira.getText();
        textCur = quantCurandeiro.getText();
        textMer = quantMercenario.getText();
        if (!"".equals(textCav)){
            int qCavaleiro = Integer.parseInt(textCav);
            custo += qCavaleiro*cCav;
        }
        if (!"".equals(textArq)){
            int qArqueira = Integer.parseInt(textArq);
            custo += qArqueira*cArq;
        }
        if (!"".equals(textCur)){
            int qCurandeiro = Integer.parseInt(textCur);
            custo += qCurandeiro*cCur;
        }
        if (!"".equals(textMer)){
            int qMercenario = Integer.parseInt(textMer);
            custo += qMercenario*cMer;
        }
        dinheiroTela.setText (Integer.toString((dinheiro-custo)));
        return custo;
    }

    /**
     * Método que troca para a tela de configuração do cavaleiro, chamando o método trocaTelaPersonagens e passando como parâmetro a unidade cavaleiro. 
     */
    public void configurarCavaleiro(){
        Mercantilismo.trocaTelaPersonagens("Configurar.fxml", "cavaleiro");
    }    

    /**
     * Método que troca para a tela de configuração da arqueira, chamando o método trocaTelaPersonagens e passando como parâmetro a unidade arqueira.
     */
    public void configurarArqueira(){
        Mercantilismo.trocaTelaPersonagens("Configurar.fxml", "arqueira");
    }

    /**
     * Método que troca para a tela de configuração do curandeiro, chamando o método trocaTelaPersonagens e passando como parâmetro a unidade curandeiro.
     */
    public void configurarCurandeiro(){
        Mercantilismo.trocaTelaPersonagens("Configurar.fxml", "curandeiro");
    }

    /**
     * Método que troca para a tela de configuração do mercenário, chamando o método trocaTelaPersonagens e passando como parâmetro a unidade mercenario.
     */
    public void configurarMercenario(){
        Mercantilismo.trocaTelaPersonagens("Configurar.fxml", "mercenario");
    }

    /**
     * Método que troca para a tela de informações do curandeiro, chamando o método trocaTelaStatus e passando como parâmetro a unidade cavaleiro.
     */
    public void statusCavaleiro(){
       Mercantilismo.trocaTelaPersonagens("StatusPersonagens.fxml", "cavaleiro");
    }  

    /**
     * Método que troca para a tela de informações do curandeiro, chamando o método trocaTelaStatus e passando como parâmetro a unidade arqueira.
     */
    public void statusArqueira(){
        Mercantilismo.trocaTelaPersonagens("StatusPersonagens.fxml", "arqueira");
    }  

    /**
     * Método que troca para a tela de informações do curandeiro, chamando o método trocaTelaStatus e passando como parâmetro a unidade curandeiro.
     */
    public void statusCurandeiro(){
        Mercantilismo.trocaTelaPersonagens("StatusPersonagens.fxml", "curandeiro");
    }  

    /**
     * Método que troca para a tela de informações do curandeiro, chamando o método trocaTelaStatus e passando como parâmetro a unidade mercenario.
     */
    public void statusMercenario(){
        Mercantilismo.trocaTelaPersonagens("StatusPersonagens.fxml", "mercenario");
    }  
}
